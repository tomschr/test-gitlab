#!/usr/bin/env python3


def hello(greet):
    return "Hello {}".format(greet)


if __name__ == "__main__":
    print(hello("world"))
